import logo from './logo.svg';
import './App.css';
import { DataProvider } from './components/DataProvider';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React, { useState } from "react";
import Home from './components/Home';
import TableData from './components/TableData';
import FormData from './components/FormData';


function App() {
 
  return (
    <DataProvider>
    <div className="App">
    <Router>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
                <Route exact path="/tabledata" element={<TableData/>}/>
                <Route exact path="/formdata" element={<FormData/>}/>
             </Routes>
        </Router>
    </div>
    </DataProvider>
  );
}

export default App;
