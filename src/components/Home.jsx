import React from 'react'
import { Link } from "react-router-dom";

function Home() {
  return (
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-info">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
      <Link
              className="nav-item nav-link active"
              to="/"
            >
             <b style={{color:"black"}}> Home</b>
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/tabledata"
            >
             <b style={{color:"black"}}> TableData</b>
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/formdata"
            >
             <b style={{color:"black"}}> Form Data</b>
            </Link>
        {/* <li class="nav-item">
          <a class="nav-link" href="#">Features</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Pricing</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled">Disabled</a>
        </li> */}
      </ul>
    </div>
  </div>
</nav>
<div>
        
        </div>
    </div>
   
  )
}

export default Home