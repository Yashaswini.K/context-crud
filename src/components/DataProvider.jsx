import React, {useState,createContext} from 'react'

export const DataContext = createContext();
export const DataProvider=(props)=> {
    
    const [record,setRecord] = useState({
        username: "",
        email: "",
        address: "",
        phone: "",
        gender: "",
        country: "",
        btechquly: false,
        mtechquly: false,
      })
      const [tablevalue,setTableValue]=useState([])
      const[id,setId]=useState('')

   
    return (
        <DataContext.Provider value={[record,setRecord,
            tablevalue, setTableValue, id,setId]}>
            {props.children}
            </DataContext.Provider>
    )
}
